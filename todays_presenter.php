<?php
// 内部文字エンコーディングをUTF-8に設定
mb_internal_encoding("UTF-8");
// time zone設定
date_default_timezone_set('Asia/Tokyo');

$debug = false;

/*** 必要なグローバル変数 ***/
// 曜日
$week_jp = array('月', '火', '水', '木', '金', '土', '日');
// APIトークン
$api_token = "8e78f9ece9f937ff532f178f5b0ced52";
// ルームID
$room_id = array('77563925', '78327222', '77570487', '77830530', '77831977');


/***　メンバー選定　***/
// DBから割り振れるメンバーを取得
$member = getPresentableSwimmy();
// 全員話し終えた場合、スイミーを全員取得
if(0 === count($member)) {
	$member = getAllSwimmy();
}

$presenter = array();
// メンバーをランダムに並べ替え
while(0 < count($member)) {
	$max = count($member) - 1;
	$id = rand(0, $max);
	array_push($presenter, $member[$id]);
	array_splice($member, $id, 1);
}

// toIDを取得
$to_id = getToId($presenter);


/***　メッセージ作成　***/
// 日付
$tomorrow = date('Y-m-d', strtotime('+1 day'));
$week = $week_jp[date("w")];

// 本文
$body = <<<EOD
[To:{$to_id[0]}]{$presenter[0]['name']} さん\n
EOD;

for($i=1;$i<3;$i++) {
	if(NULL !== $presenter[$i]['name']) {
		$body .= <<<EOD
[To:{$to_id[$i]}]{$presenter[$i]['name']} さん\n
EOD;
	}else {
		break;
	}
}

if($debug) {
$body .= <<<EOD
*** テストです ***
EOD;
}

$body .= <<<EOD
お疲れ様でございマス。

明日 {$tomorrow} ({$week}) の発表者はあなたデス!!
・{$presenter[0]['name']} サン
頑張ってくだサイ...!!

また、発表者の繰り上がり順は以下の通りデス。\n
EOD;

for($i=1;$i<count($presenter);$i++) {
	$body .= <<<EOD
{$i} : {$presenter[$i]['name']} サン\n
EOD;
}

$body .= <<<EOD
宜しくお願いしマス。

※都合の合わない場合は、名前の挙がっている順に発表者が繰り上がりマス。
※選ばれた人は、みんな一巡するまでは選ばれまセン。
後々、仕様変更予定デスが....
※毎日16:30に投稿しマス。
土日の区別がまだ無いので、365日投げ続けマス。
EOD;


/***　送信部分　***/
// ヘッダ
header("Content-type: text/html; charset=utf-8");
// POST送信データ
$params = array(
    'body' => $body
);
// cURLに渡すオプションを設定
$options = array(
    CURLOPT_URL => "https://api.chatwork.com/v2/rooms/{$room_id[0]}/messages",
    CURLOPT_HTTPHEADER => array('X-ChatWorkToken: '. $api_token),
	// 結果を文字列で返す
    CURLOPT_RETURNTRANSFER => true,
	// サーバー証明書の検証を行わない
    CURLOPT_SSL_VERIFYPEER => false,
	// HTTP POSTを実行
    CURLOPT_POST => true,
	// POST送信データ
    CURLOPT_POSTFIELDS => http_build_query($params, '', '&'),
);
// cURLセッションを初期化
$ch = curl_init();
// cURL転送用の複数のオプションを設定
curl_setopt_array($ch, $options);
// cURLセッションを実行
$response = curl_exec($ch);
// cURLセッションをクローズ
curl_close($ch);
// 結果のJSON文字列をデコード
$result = json_decode($response);
// 結果を出力 (メッセージID返ってきてる)
// var_dump($result);


/***　DB関連　***/
function getPresentableSwimmy() {
	$sdb = 'mysql:dbname=swimmy_project;host=localhost;charset=utf8';
	$username = 'root';
	$password = 'qFKmc@Q6H';
	$pdo = new PDO($sdb, $username, $password, array(PDO::ATTR_EMULATE_PREPARES => false,PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"));
	$stmt = $pdo->prepare('SELECT name FROM swimmy WHERE talk_flag <> 1');
	$stmt->execute();

	$users = $stmt -> fetchAll(PDO::FETCH_ASSOC);
	return $users;
}

function getAllSwimmy() {
	$sdb = 'mysql:dbname=swimmy_project;host=localhost;charset=utf8';
	$username = 'root';
	$password = 'qFKmc@Q6H';
	$pdo;
	$pdo = new PDO($sdb, $username, $password, array(PDO::ATTR_EMULATE_PREPARES => false,PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"));
	$stmt = $pdo->prepare('SELECT name FROM swimmy');
	$stmt->execute();
	$users = $stmt -> fetchAll(PDO::FETCH_ASSOC);

	foreach($users as $person) {
		$stmt = $pdo->prepare("update swimmy set talk_flag = 0, talk_date = NULL where name = '{$person['name']}'");
		$stmt->execute();
	}

	return $users;
}

function getToId($p) {
	$result = array();
	$sdb = 'mysql:dbname=swimmy_project;host=localhost;charset=utf8';
	$username = 'root';
	$password = 'qFKmc@Q6H';
	$pdo;
	$pdo = new PDO($sdb, $username, $password, array(PDO::ATTR_EMULATE_PREPARES => false,PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"));
	foreach($p as $person) {
		$sql = "SELECT to_id FROM swimmy WHERE name = '{$person['name']}'";
		$stmt = $pdo->prepare($sql);
		$stmt->execute();
		$id = $stmt -> fetchAll(PDO::FETCH_ASSOC);
		array_push($result, $id[0]['to_id']);
	}
	return $result;
}

?>
