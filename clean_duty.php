<?php
// time zone設定
date_default_timezone_set('Asia/Tokyo');

$week_jp = array('日', '月', '火', '水', '木', '金', '土');
// APIトークン
$api_token = "8e78f9ece9f937ff532f178f5b0ced52";
// ルームID
$room_id   = array('77563925', '78327222', '77570487', '77830530', '77831977');
// スイミー
$member = array('阿久津', '近藤', '高橋', '三木', '青木', '伊藤', '尾崎', '舘野', '馬場', '山下', '三原');
// 掃除場所
$places = array('トイレ(男)' => 2, 'トイレ(女)' => 1, '神棚・水やり' => 1, '寝室' => 1, '会議室(A,B,C,役会,応接)' => 2, '地下倉庫' => 4);


/*********
 * メッセージ作成
 *********/
// 日付
$today = date("Y/m/d");
$week = $week_jp[date("w")];

// メッセージ本文
$new_member = array();
// 男子トイレ
$id = rand(0, 3);
array_push($new_member, $member[$id]);
array_splice($member, $id, 1);
$id = rand(0, 2);
array_push($new_member, $member[$id]);
array_splice($member, $id, 1);
// 女子トイレ
$id = rand(2, 8);
array_push($new_member, $member[$id]);
array_splice($member, $id, 1);
// 神棚
$id = rand(0, 7);
array_push($new_member, $member[$id]);
array_splice($member, $id, 1);
// 残り
$times = count($member);
for($i=1;$i<$times;$i++) {
	$max = count($member) - 1;
	$id = rand(0, $max);
	array_push($new_member, $member[$id]);
	array_splice($member, $id, 1);
}
array_push($new_member, $member[0]);

// 今日の気分
$todays_feeling = array(":)", ":(", ":D", ";(", "|-)", ":p", "(devil)", "(dance)");
$emotion = rand(0, 7);

// 本文
$body = <<<EOD
おはようございマス!
今日の気分はこんな感じ。{$todays_feeling[$emotion]}

今日の掃除当番デス。[info][title]{$today} ({$week}) スイミー掃除当番表[/title]
EOD;

$c = 0;
foreach($places as $name => $val) {
		$body .= <<<EOS
【{$name}】\n
EOS;
	for($i=0;$i<$val;$i++) {
		$body .= <<<EOS
{$new_member[$c]}
EOS;
		if(($val - 1) !== $i) {
		$body .= <<<EOS
・
EOS;
		} else {
		$body .= <<<EOS
\n
EOS;
		}
		$c++;
	}
}

$body .= <<<EOD
[/info]都合の合わない人は、どなたかに交代をお願いしてくだサイ。
宜しくお願いしマス。

※毎朝8時に投稿しマス。
EOD;

/*********
 * 送信部分
 *********/
// ヘッダ
header("Content-type: text/html; charset=utf-8");
// POST送信データ
$params = array(
    'body' => $body
);
// cURLに渡すオプションを設定
$options = array(
    CURLOPT_URL => "https://api.chatwork.com/v2/rooms/{$room_id[0]}/messages",
    CURLOPT_HTTPHEADER => array('X-ChatWorkToken: '. $api_token),
	// 結果を文字列で返す
    CURLOPT_RETURNTRANSFER => true,
	// サーバー証明書の検証を行わない
    CURLOPT_SSL_VERIFYPEER => false,
	// HTTP POSTを実行
    CURLOPT_POST => true,
	// POST送信データ
    CURLOPT_POSTFIELDS => http_build_query($params, '', '&'),
);
// cURLセッションを初期化
$ch = curl_init();
// cURL転送用の複数のオプションを設定
curl_setopt_array($ch, $options);
// cURLセッションを実行
$response = curl_exec($ch);
// cURLセッションをクローズ
curl_close($ch);
// 結果のJSON文字列をデコード
$result = json_decode($response);
// 結果を出力 (メッセージID返ってきてる)
// var_dump($result);
?>
