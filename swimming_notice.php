<?php
// 内部文字エンコーディングをUTF-8に設定
mb_internal_encoding("UTF-8");
// time zone設定
date_default_timezone_set('Asia/Tokyo');

// APIトークン
$api_token = "8e78f9ece9f937ff532f178f5b0ced52";
// ルームID
$room_id = '77208649';

$date = date("Y-m-d");
// 本文
$body = <<<EOD
${date}
本日の報告は以下から
https://docs.google.com/forms/d/e/1FAIpQLSdOjsyiRMfaWEArJcv4BbouMFe-RM_mQ9lgb6OLZkliungTpw/viewform

回答を見るには以下から
https://docs.google.com/spreadsheets/d/1ihEuKLvw8mK4ur0i1vkGzl7BEZzwF_EmcsPQEC7zB90/edit#gid=767255471
EOD;


/***　送信部分　***/
// ヘッダ
header("Content-type: text/html; charset=utf-8");
// POST送信データ
$params = array(
    'body' => $body
);
// cURLに渡すオプションを設定
$options = array(
    CURLOPT_URL => "https://api.chatwork.com/v2/rooms/{$room_id}/messages",
    CURLOPT_HTTPHEADER => array('X-ChatWorkToken: '. $api_token),
	// 結果を文字列で返す
    CURLOPT_RETURNTRANSFER => true,
	// サーバー証明書の検証を行わない
    CURLOPT_SSL_VERIFYPEER => false,
	// HTTP POSTを実行
    CURLOPT_POST => true,
	// POST送信データ
    CURLOPT_POSTFIELDS => http_build_query($params, '', '&'),
);
// cURLセッションを初期化
$ch = curl_init();
// cURL転送用の複数のオプションを設定
curl_setopt_array($ch, $options);
// cURLセッションを実行
$response = curl_exec($ch);
// cURLセッションをクローズ
curl_close($ch);
// 結果のJSON文字列をデコード
$result = json_decode($response);
// 結果を出力 (メッセージID返ってきてる)
// var_dump($result);


/***　DB関連　***/
function getAllSwimmy() {
	$sdb = 'mysql:dbname=swimmy_project;host=localhost;charset=utf8';
	$username = 'root';
	$password = 'qFKmc@Q6H';
	$pdo;
	$pdo = new PDO($sdb, $username, $password, array(PDO::ATTR_EMULATE_PREPARES => false,PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"));
	$stmt = $pdo->prepare('SELECT name, to_id FROM swimmy');
	$stmt->execute();
	$users = $stmt -> fetchAll(PDO::FETCH_ASSOC);

	return $users;
}

?>
