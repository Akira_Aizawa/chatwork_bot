require "selenium-webdriver"

driver = Selenium::WebDriver.for :chrome

# chatwork login
driver.navigate.to 'https://www.chatwork.com/login.php?args='

p 'Login to Chatwork...'
id = driver.find_element(:id, "login_email")
pass = driver.find_element(:name, 'password')
id.send_keys("swimmy.uf@gmail.com")
pass.send_keys("2017swimmy")
# id.send_keys("akira.aizawa@urban-funes.co.jp")
# pass.send_keys("aizawajoc2689")
driver.find_element(:class, 'new-theme_section__inner__row').find_element(:name, 'login').click

# 煕(スイミーちゃん)
driver.navigate.to 'https://www.chatwork.com/#!rid77734346'
# 三木(スイミーちゃん)
# driver.navigate.to 'https://www.chatwork.com/#!rid77732166'
# 近藤(スイミーちゃん)
# driver.navigate.to 'https://www.chatwork.com/#!rid74661218'
sleep 5

p 'Writing...'
driver.find_element(:id, '_to').click
driver.find_element(:id, '_toList').find_element(:tag_name, 'li').click

random = Random.new

driver.find_element(:id, '_chatText').send_keys("それでは、これよりお体をお守りするために\nドライアイスのご処置と枕飾りと呼ばれる、\nお参りの準備をいたします。\n")
driver.find_element(:id, '_chatText').send_keys("恐れ入りますが、ご宗派を教えていただけませんか?\n曹洞宗ですね。\nかしこまりました。\n")
driver.find_element(:id, '_chatText').send_keys("\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1
p 'Writing...'
driver.find_element(:id, '_chatText').send_keys("それでは、まずドライアイスのご処置をさせていただきます。\n")
driver.find_element(:id, '_chatText').send_keys("\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1
p 'Writing...'
driver.find_element(:id, '_chatText').send_keys("つづきまして、枕飾りの準備をいたします。\n少々お時間を頂きますので、お足元を楽にしてお待ちください。\n")
driver.find_element(:id, '_chatText').send_keys("\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1
p 'Writing...'
driver.find_element(:id, '_chatText').send_keys("こちらは「末期の水」でございます。\nお父様は病院で末期の水をお取りになりましたでしょうか?\nかしこまりました。\n")
driver.find_element(:id, '_chatText').send_keys("それでは、こちらの器にうすくお水をはってお持ち頂けますでしょうか。\nまた、お参り用の座布団とコップ一杯のお水をご用意ください。\n")
driver.find_element(:id, '_chatText').send_keys("大変お待たせいたしました。\n準備が整いましたので、これよりお参りの説明をいたします。\n恐れ入りますが、皆様お集まりください。\n")
driver.find_element(:id, '_chatText').send_keys("\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1
p 'Writing...'
driver.find_element(:id, '_chatText').send_keys("まず、「末期の水」について、説明いたします。\n")
driver.find_element(:id, '_chatText').send_keys("お釈迦様がお亡くなりになる前に「喉が渇いた」と言われ、お弟子さんが急いで水を汲みに行ったのですが、\nお釈迦様はその水を飲む前、喉が渇いたまま、お亡くなりになられた\nという言い伝えから、人が亡くなってから最初に欲するのが「水」と言われております。\n")
driver.find_element(:id, '_chatText').send_keys("お父様があの世で渇きに苦しまないように、こちらの綿棒にお水を軽く浸していただき、お父様のお口元を軽くなでて潤してあげてください。\nお済みになりましたら、続いてお線香を差し上げてください。\n")
driver.find_element(:id, '_chatText').send_keys("\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1
p 'Writing...'
driver.find_element(:id, '_chatText').send_keys("では、血縁の近い方から順番にお願いいたします。\nお次の方、どうぞ。\n")
driver.find_element(:id, '_chatText').send_keys("\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1
p 'Writing...'
driver.find_element(:id, '_chatText').send_keys("皆様、お済みになりましたか?\n末期の水は、お一人様一回となりますので、この後ご身内の方がご弔問にいらっしゃいましたら末期の水を差し上げてください。\nお疲れさまでした。\n")
p 'Sending...'
driver.find_element(:id, '_sendButton').click
sleep 1

p 'Done!'
driver.quit