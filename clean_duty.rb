require "selenium-webdriver"
require "date"

$round = 8
$member = ['愛澤', '阿久津', '近藤', '高橋', '三木', '青木', '伊藤', '尾崎', '舘野', '馬場', '山下', '三原']

def main()
	p 'Hello!!'
	print 'Please input Chat Group : '
	mode = gets.chomp()

	# ローカルテスト用
	driver = Selenium::WebDriver.for :chrome
	# サーバ用
	# driver = Selenium::WebDriver.for :firefox

	login(driver)

	# 投稿するグループを選択
	case mode
	when '0' then
		# スイミーちゃんと一緒に移動
		driver.navigate.to 'https://www.chatwork.com/#!rid77563925'
	when '1' then
		# スイミーちゃんテスト1に移動
		driver.navigate.to 'https://www.chatwork.com/#!rid78327222'
	when '2' then
		# スイミーちゃんテスト2に移動
		driver.navigate.to 'https://www.chatwork.com/#!rid77570487'
	when '3' then
		# スイミーちゃんテスト3に移動
		driver.navigate.to 'https://www.chatwork.com/#!rid77830530'
	when '4' then
		# スイミーちゃんテスト4に移動
		driver.navigate.to 'https://www.chatwork.com/#!rid77831977'
	else
		p 'Wrong.'
		exit();
	end

	# 掃除に出れない人を選択
	$member.each_with_index do |m, i|
		print "[#{i} : #{m}] "
	end
	while true
		d = gets.chomp()
		if 'q' == d then
			break
		else
			$member.delete_at(d.to_i)
		end
	end



	begin
		sleep 3

		touban = Array.new()
		$round.times do
			touban = fixCleaner(mode)
			sleep(0.5)
		end

		places = [["トイレ(男)", 2], ["トイレ(女)", 1], ["神棚・水やり", 1], ["寝室", 1], ["会議室(A,B,C,役会,応接)", 2], ["地下倉庫", 4], ["コーヒースペース", 1]]

		p 'Writing...'

		d = Date.today
		d = d +
		today = d.strftime("%Y/%m/%d (%a)")

		chatArea = driver.find_element(:id, '_chatText')
		case mode
		when '0' then
			chatArea.send_keys("おはようございマス。\n今日の掃除当番デス。[info][title]" + today + ' スイミー掃除当番表[/title]')
		else
			chatArea.send_keys("** テストです **\nおはようございマス。\n今日の掃除当番デス。[info][title]" + today + ' スイミー掃除当番表[/title]')
		end

		n = 0
		flg = 0
		places.each do |place, i|
			chatArea.send_keys('【' + place + "】\n")
			i.times do
				if 0 == flg then
					chatArea.send_keys('　' + touban[n])
					flg = 1
				else
					chatArea.send_keys('・' + touban[n])
				end
				n = n + 1
			end
			chatArea.send_keys("\n")
			flg = 0
		end
		chatArea.send_keys("[/info]都合の合わない人は、どなたかに交代をお願いしてくだサイ。\n宜しくお願いしマス。")
		sleep 0.5
		p 'Done!'

		p 'Sending...'
		driver.find_element(:id, '_sendButton').click
		sleep 0.5

		p 'Done!'
		driver.quit
	rescue
		driver.quit
	end
end

def login(driver)
	# chatwork login
	driver.navigate.to 'https://www.chatwork.com/login.php?args='

	p 'Login to Chatwork...'
	id = driver.find_element(:id, "login_email")
	pass = driver.find_element(:name, 'password')
	id.send_keys("swimmy.uf@gmail.com")
	pass.send_keys("2017swimmy")
	driver.find_element(:class, 'new-theme_section__inner__row').find_element(:name, 'login').click
	p 'Done!'
end

def fixCleaner(mode)
	rnd = Random.new
	res = Array.new()

	# トイレ
	id = rnd.rand(0..4)
	res.push($member[id])
	$member.delete_at(id)

	id = rnd.rand(0..3)
	res.push($member[id])
	$member.delete_at(id)

	id = rnd.rand(3..9)
	res.push($member[id])
	$member.delete_at(id)

	# 神棚
	id = rnd.rand(1..7)
	res.push($member[id])
	$member.delete_at(id)

	n = $member.length
	n.times do
		id = rnd.rand($member.length)
		res.push($member[id])
		$member.delete_at(id)
	end

	return res
end

main()
